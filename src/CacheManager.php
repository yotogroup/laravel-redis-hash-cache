<?php

namespace Yoto\LaravelRedisHashCache;

/**
 * @mixin \Illuminate\Contracts\Cache\Repository
 */
class CacheManager extends \Illuminate\Cache\CacheManager
{
    /**
     * Create an instance of the Redis cache driver.
     *
     * @param  array  $config
     * @return \Yoto\LaravelRedisHashCache\RedisHashStore|\Illuminate\Cache\Repository
     */
    protected function createRedis_hashDriver(array $config)
    {
        $redis = $this->app['redis'];

        $hashKey = $config['hash_key'] ?? 'hash';
        $connection = $config['connection'] ?? 'default';

        return $this->repository(new RedisHashStore($redis, $this->getPrefix($config), $connection, $hashKey));
    }
}
